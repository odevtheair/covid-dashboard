declare let L: any;

import { Component, ViewChild, Renderer2, OnInit } from '@angular/core';
import { Router, RouterEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { CovidService } from './shared/covid.service';
import { ToastrService } from 'ngx-toastr';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label, Color, MultiDataSet } from 'ng2-charts';
import * as moment from 'moment';
import * as _ from 'lodash';
import * as Chart from 'chart.js';
import { ModalPatientComponent } from './modal-patient/modal-patient.component';

moment.locale('th');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private map;
  groupProvince: any = [];

  public sidebarMenuOpened = true;
  @ViewChild('contentWrapper', { static: false }) contentWrapper;
  @ViewChild('mdlPatient') mdlPatient: ModalPatientComponent;

  page: number = 1;
  pageN: number = 1;
  Male = 0;
  Female = 0;
  Unknown = 0;

  provinces = [];
  nations = [];
  cases = [];

  Confirmed = 0;
  Recovered = 0;
  Hospitalized = 0;
  Deaths = 0;
  NewConfirmed = 0;
  NewRecovered = 0;
  NewHospitalized = 0;
  NewDeaths = 0;
  UpdateDate = '';

  loading: boolean;

  public doughnutChartLabels: Label[] = ['ชาย', 'หญิง', 'ไม่ทราบ'];
  public doughnutChartData: ChartDataSets[] = [
    {
      data: [],
      label: 'เพศ',
      backgroundColor: [
        '#ff6384',
        '#36a2eb',
        '#ffc107'
      ]
    }
  ];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartOption: ChartOptions = {
    maintainAspectRatio: false,
    title: {
      display: true,
      text: 'แยกตามเพศ',
      fontSize: 18
    },
    responsive: true,
    legend: {
      position: 'bottom',
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  }

  public lineChartData: ChartDataSets[] = [
    {
      data: [],
      label: 'ผู้ป่วยสะสม',
      fill: true,
    },
    {
      data: [],
      label: 'รักษาหาย',
      fill: true,
    },
    {
      data: [],
      label: 'รักษาตัวที่ รพ.',
      fill: true,
    },
    {
      data: [],
      label: 'เสียชีวิต',
      fill: true,
    },
  ];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: ChartOptions = {
    title: {
      display: true,
      text: 'สถิติประจำวัน (ตั้งแต่เดือน เม.ย.)',
      fontSize: 18
    },

    responsive: true,
    scales: {
      yAxes: [{
        gridLines: {
          drawBorder: false
        },
        scaleLabel: {
          display: true,
          labelString: 'จำนวนผู้ป่วยสะสม (ราย)'
        }
      }],
      xAxes: [{
        type: 'time',
        time: {
          unit: 'day'
        },
        distribution: 'series',
        offset: true,
        ticks: {
          major: {
            enabled: true,
            fontStyle: 'bold'
          },
          source: 'data',
          autoSkip: true,
          autoSkipPadding: 75,
          maxRotation: 0,
          sampleSize: 100
        },
      }]
    }
  };
  public lineChartColors: Color[] = [];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  // top 10 province
  public barChartOptions: ChartOptions = {
    title: {
      display: true,
      text: '20 อันดับจังหวัดที่มีผู้ป่วยสะสมมากที่สุด',
      // fontSize: 18
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: { xAxes: [{}], yAxes: [{}] },
    tooltips: {
      callbacks: {
        label: function (tooltipItem: any, data: any) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          label += tooltipItem.xLabel.toLocaleString() + ' ราย';
          return label;
        }
      }
    },
    animation: {
      duration: 1,
      onComplete: function () {
        var chartInstance = this.chart,
          ctx = chartInstance.ctx;

        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
        ctx.textAlign = 'center';
        ctx.textBaseline = 'top';

        this.data.datasets.forEach(function (dataset: any, i: any) {
          var meta = chartInstance.controller.getDatasetMeta(i);
          meta.data.forEach(function (bar: any, index: any) {
            var data = dataset.data[index].toLocaleString();
            ctx.fillText(data, bar._model.x, bar._model.y - 5);
          });
        });
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'horizontalBar';
  public barChartLegend = false;

  public barChartData: ChartDataSets[] = [
    { data: [], label: 'ผู้ป่วยสะสม' },
  ];

  // top 10 province
  public wbarChartOptions: ChartOptions = {

    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{}], yAxes: [{
        gridLines: {
          drawBorder: false
        },
        scaleLabel: {
          display: true,
          labelString: 'จำนวน (ราย)'
        }
      }]
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem: any, data: any) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
          }
          label += tooltipItem.yLabel.toLocaleString() + ' ราย';
          return label;
        }
      }
    },
  };
  public wbarChartLabels: Label[] = [];
  public wbarChartType: ChartType = 'bar';
  public wbarChartLegend = true;

  public wbarChartData: ChartDataSets[] = [
    { data: [], label: 'ผู้ป่วยสะสม' },
    { data: [], label: 'เสียชีวิต' },
    { data: [], label: 'รักษาหาย' },
  ];


  patients = [];
  countries = [];
  allGlobal: any = {};

  constructor(
    private router: Router,
    private renderer: Renderer2,
    private covidService: CovidService,
    private toastr: ToastrService
  ) {
    this.loading = false;
    router.events.subscribe(
      (event: RouterEvent): void => {
        if (event instanceof NavigationStart) {
          this.loading = true;
        } else if (event instanceof NavigationEnd) {
          this.loading = false;
        }
      }
    );
  }

  async ngOnInit() {
    await this.getToday();
    await this.getCases();
    await this.getTimeline();
    await this.getSum();

    this.getByCountries();
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  private async initMap() {
    this.map = L.map('map', {
      center: [13.7245601, 100.4930267],
      zoom: 6
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 15,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);

  }

  async initialGeoJSON() {
    try {
      const _geoJson: any = await this.covidService.getGeoJSON();
      const dataJson = _geoJson;
      for (var i = 0; i < dataJson.features.length; i++) {
        if (this.groupProvince[dataJson.features[i]['properties']['name']]) {
          if (this.groupProvince[dataJson.features[i]['properties']['name']]['last14_count'] > 0) {
            dataJson.features[i]['properties']['density'] = this.groupProvince[dataJson.features[i]['properties']['name']]['last14_count']
          } else {
            dataJson.features[i]['properties']['density'] = -1
          }
        } else {
          dataJson.features[i]['properties']['density'] = 0
        }

      }

      var geoJson = L.geoJSON(dataJson.features, {
        style: this.style,
        onEachFeature: (feature: any, layer: any) => {
          // layer.bindPopup(feature.properties.name);
          layer.on({
            mouseover: (e: any) => {
              var layer = e.target;
              layer.setStyle({
                weight: 2,
                color: '#000',
                dashArray: '',
                fillOpacity: 0.8
              });

              if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
              }

            },
            mouseout: (e: any) => {
              geoJson.resetStyle(e.target);
            },
            click: (e: any) => {
              this.openInfo(e.target.feature.properties);
            }

          });
        }
      }).addTo(this.map);

    } catch (error) {
      console.log(error);
      this.toastr.error(error.message);
    }
  }

  openInfo(properties: any) {
    const provinceName = properties.thname;
    const data: any = [];

    this.cases.forEach(v => {
      if (v.Province === provinceName) {
        data.push(v);
      }
    });
    this.mdlPatient.open(provinceName, data);
  }

  style(feature: any) {
    return {
      weight: 1,
      opacity: 0.7,
      color: '#666',
      fillColor: feature.properties.density > 100 ? '#870a30' :
        feature.properties.density >= 50 ? '#ff2121' :
          feature.properties.density >= 10 ? '#ff4141cb' :
            feature.properties.density >= 5 ? '#ff8746' :
              feature.properties.density >= 1 ? '#fdb64a' :
                feature.properties.density == -1 ? '#ff9a29' :
                  '#28a745',
      dashArray: '1',
      fillOpacity: 0.5,
    };
  }

  async getToday() {
    this.loading = true;
    try {
      const rs: any = await this.covidService.getToday();
      this.Confirmed = rs.Confirmed;
      this.Deaths = rs.Deaths;
      this.Hospitalized = rs.Hospitalized;
      this.NewConfirmed = rs.NewConfirmed;
      this.NewDeaths = rs.NewDeaths;
      this.NewHospitalized = rs.NewHospitalized;
      this.NewRecovered = rs.NewRecovered;
      this.Recovered = rs.Recovered;
      this.UpdateDate = rs.UpdateDate;
      this.loading = false;
    } catch (error) {
      this.loading = false;
      console.log(error);
      this.toastr.error();
    }
  }

  async getCases() {
    this.loading = true;
    try {
      const rs: any = await this.covidService.getCases();

      this.cases = rs.Data;
      const dataCases = rs.Data;

      for (var i = 0; i < dataCases.length; i++) {
        var data = dataCases[i];
        let _name = data['ProvinceEn'].split(" ").join("")
        if (typeof this.groupProvince[_name] !== 'undefined') {
          this.groupProvince[_name]['count']++;
          this.groupProvince[_name]['provinceId'] = dataCases[i].ProvinceId;

          if (moment().diff(data['ConfirmDate'], 'days') <= 7) {
            this.groupProvince[_name]['last7_count']++;
          } else {
            this.groupProvince[_name]['over7_count']++;
          }
          if (moment().diff(data['ConfirmDate'], 'days') <= 14) {
            this.groupProvince[_name]['last14_count']++;
          } else {
            this.groupProvince[_name]['over14_count']++;
          }
          if (moment().diff(data['ConfirmDate'], 'days') <= 1) {
            this.groupProvince[_name]['last_count']++;
          }
          if (data['GenderEn'] == 'Female') {
            this.groupProvince[_name]['Female']++

          } else if (data['GenderEn'] == 'Male') {
            this.groupProvince[_name]['Male']++
          }

          if (data['NationEn'] == 'Thai') {
            this.groupProvince[_name]['Thai']++
          } else if (data['GenderEn'] != '' || data['GenderEn'] != 'Unknown') {
            this.groupProvince[_name]['Foreigner']++
          }
          // console.log(data['age'])
          if (data['Age'] != '' && data['Age'] != 'Unknown' && data['Age'] != 0 && data['Age'] != 'null' && data['Age'] != null) {
            this.groupProvince[_name]['CountWithAge']++;
            this.groupProvince[_name]['SumAge'] += data['Age'];
          }

          // groupProvince[_name]['data'].push(data)
        }
        else {
          this.groupProvince[_name] = {};
          this.groupProvince[_name]['data'] = [];
          this.groupProvince[_name]['name'] = data['Province'];
          this.groupProvince[_name]['Male'] = 0;
          this.groupProvince[_name]['Female'] = 0;
          this.groupProvince[_name]['Thai'] = 0;
          this.groupProvince[_name]['Foreigner'] = 0;
          this.groupProvince[_name]['SumAge'] = 0;
          this.groupProvince[_name]['CountWithAge'] = 0;
          // this.groupProvince[_name]['lat_long'] = arrLat[data['ProvinceId'] - 1];
          this.groupProvince[_name]['count'] = 1;
          this.groupProvince[_name]['last7_count'] = 0;
          this.groupProvince[_name]['over7_count'] = 0;
          if (moment().diff(data['ConfirmDate'], 'days') <= 7) {
            this.groupProvince[_name]['last7_count'] = 1;
          } else {
            this.groupProvince[_name]['over7_count'] = 1;
          }
          this.groupProvince[_name]['last14_count'] = 0;
          this.groupProvince[_name]['over14_count'] = 0;
          if (moment().diff(data['ConfirmDate'], 'days') <= 14) {
            this.groupProvince[_name]['last14_count'] = 1;
          } else {
            this.groupProvince[_name]['over14_count'] = 1;
          }
          this.groupProvince[_name]['last_count'] = 0;
          if (moment().diff(data['ConfirmDate'], 'days') <= 1) {
            this.groupProvince[_name]['last_count'] = 1;
          }
          this.groupProvince[_name]['ProvinceId'] = data['ProvinceId'];
          if (data['GenderEn'] == 'Female') {
            this.groupProvince[_name]['Female']++
          } else if (data['GenderEn'] == 'Male') {
            this.groupProvince[_name]['Male']++
          }
          if (data['NationEn'] == 'Thai') {
            this.groupProvince[_name]['Thai']++
          } else if (data['GenderEn'] != '' || data['GenderEn'] != 'Unknown') {
            this.groupProvince[_name]['Foreigner']++
          }
          if (data['Age'] != '' && data['Age'] != 'Unknown' && data['Age'] != 0 && data['Age'] != 'null' && data['Age'] != null) {
            this.groupProvince[_name]['CountWithAge']++;
            this.groupProvince[_name]['SumAge'] = data['Age'];
          }
        }
      }

      //

      await this.initialGeoJSON();

      this.loading = false;
    } catch (error) {
      this.loading = false;
      console.log(error);
      this.toastr.error();
    }
  }

  async getSum() {
    this.loading = true;
    try {
      const rs: any = await this.covidService.getSum();

      _.forEach(rs.Province, (k: any, v: any) => {
        const obj: any = {};
        const idx = _.findIndex(this.cases, { ProvinceEn: v });
        obj.name = idx > 0 ? this.cases[idx].Province : v;
        obj.total = +k;
        obj.provinceId = idx > 0 ? this.cases[idx].ProvinceId : 0;
        this.provinces.push(obj);
      });

      // top 10 province

      const _pData = this.provinces.slice(0, 20);

      _pData.forEach(v => {
        this.barChartData[0].data.push(v.total);
        this.barChartLabels.push(v.name);
      });

      _.forEach(rs.Nation, (k: any, v: any) => {
        const obj: any = {};
        const idx = _.findIndex(this.cases, { NationEn: v });
        obj.name = v === 'Thai' ? 'ไทย' : idx > 0 ? this.cases[idx].Nation : v;
        obj.total = +k;
        this.nations.push(obj);
      });


      let data = [];
      data.push(rs.Gender.Male);
      data.push(rs.Gender.Female);
      data.push(rs.Gender.Unknown);

      this.Male = rs.Gender.Male;
      this.Female = rs.Gender.Female;
      this.Unknown = rs.Gender.Unknown;

      this.doughnutChartData[0].data = data;
      this.loading = false;
    } catch (error) {
      this.loading = false;
      console.log(error);
      this.toastr.error('เกิดข้อผิดพลาด [SUM]');
    }
  }

  async getTimeline() {
    this.loading = true;
    try {
      const rs: any = await this.covidService.getTimeline();
      const labels = [];
      const data = [];
      const dataDeath = [];
      const dataRecovered = [];
      const dataHospitalized = [];

      rs.Data.forEach(v => {
        if (moment(v.Date, 'MM/DD/YYYY').get('month') >= 3) {
          const date = v.Date;
          labels.push(date);
          data.push(+v.Confirmed);
          dataDeath.push(+v.Deaths);
          dataRecovered.push(+v.Recovered);
          dataHospitalized.push(Math.abs(+v.Hospitalized));
        }

      });

      this.lineChartLabels = labels;
      this.lineChartData[0].data = data;
      this.lineChartData[1].data = dataRecovered;
      this.lineChartData[2].data = dataHospitalized;
      this.lineChartData[3].data = dataDeath;
      // { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }

      this.loading = false;
    } catch (error) {
      this.loading = false;
      console.log(error);
      this.toastr.error('เกิดข้อผิดพลาด [Timeline]');
    }
  }

  async getByCountries() {
    try {
      const rs: any = await this.covidService.getByCuntries();
      const data = rs.Countries;
      const items = _.orderBy(data, ['TotalConfirmed'], 'desc').slice(0, 20);
      this.allGlobal = rs.Global;
      items.forEach(v => {
        this.wbarChartData[0].data.push(v.TotalConfirmed);
        this.wbarChartData[1].data.push(v.TotalDeaths);
        this.wbarChartData[2].data.push(v.TotalRecovered);
        this.wbarChartLabels.push(v.Country);
      });
    } catch (error) {
      this.loading = false;
      console.log(error);
      this.toastr.error('เกิดข้อผิดพลาด [Countries]');
    }
  }

}
